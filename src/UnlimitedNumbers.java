import java.math.BigInteger;

public class UnlimitedNumbers {
    public static void main(String[] args) {
        String firstNumber = "123456789";
        String secondNumber = "9876543";
        System.out.println(stringAdd(firstNumber, secondNumber));
    }
    public static String stringAdd(String firstNumber, String secondNumber) {
        if (firstNumber.length() > secondNumber.length()) {
            String temporary = firstNumber;
            firstNumber = secondNumber;
            secondNumber = temporary;
        }
        int length1 = firstNumber.length();
        int length2 = secondNumber.length();
        String addResult = "";
        firstNumber = new StringBuilder(firstNumber).reverse().toString();
        secondNumber = new StringBuilder(secondNumber).reverse().toString();
        int carry = 0;
        for (int i = 0; i < length1; i++) {
            int sum = ((int) (firstNumber.charAt(i) - '0') +
                    (int) (secondNumber.charAt(i) - '0') + carry);
            addResult += (char) (sum % 10 + '0');
            carry = sum / 10;
        }
        for (int i = length1; i < length2; i++) {
            int sum = ((int) (secondNumber.charAt(i) - '0') + carry);
            addResult += (char) (sum % 10 + '0');
            carry = sum / 10;
        }
        if (carry > 0)
            addResult += (char) (carry + '0');
        addResult = new StringBuilder(addResult).reverse().toString();
        return addResult;
    }
}
