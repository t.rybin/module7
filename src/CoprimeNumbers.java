import java.util.Random;

public class CoprimeNumbers {
    public static void main(String[] args) {
        //Random number = new Random();
        //int firstNumber = number.nextInt();
        //int secondNumber = number.nextInt();
        //int thirdNumber = number.nextInt();
        int firstNumber =8;
        int secondNumber =6;
        int thirdNumber = 4;
        display(firstNumber, secondNumber, thirdNumber);
    }
    public static int checkCoprime(int firstNumber, int secondNumber, int thirdNumber){
        int gcd=0;
        for (int i =1; i<= firstNumber && i<= secondNumber && i<=thirdNumber; i++){
            if (firstNumber%i==0 && secondNumber%i==0 && thirdNumber%i==0){
                gcd = i;
            }
        }
        return gcd;
    }
    public static void display(int firstNumber, int secondNumber, int thirdNumber){
        if (checkCoprime(firstNumber, secondNumber, thirdNumber)!=1){
            System.out.println("Given numbers are not coprime");
        } else{
            System.out.println("Given numbers are coprime");
        }
    }
}
