public class ArraySum {
    public static void main(String[] args) {
        int[] D = new int[]{8,6,4,2,28,132,14,-10};
        int k = 0;
        int m = 5;
        findSum(D, k, m);
    }
    public static void findSum(int[] D, int k, int m){
        for (int i = k; i<=m; i++){
            int sum = D[i]+D[i+1]+D[i+2];
            System.out.printf("%nThe sum of %d, %d and %d is:%d", D[i], D[i+1], D[i+2], sum);
        }
    }
}
