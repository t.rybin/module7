import java.util.Arrays;

public class SuperLock {
    public static void main(String[] args) {
        int[] password = new int[10];
        password[0] = 2;
        password[1] = 6;
        checkTheDoor(password);
    }

    public static void checkTheDoor(int[] password) {
        int sum = 10;
        for (int i = 1; i < password.length - 1; i++) {
            if (i == 1) {
                while (password[i - 1] + password[i] + password[i + 1] != sum) {
                    int max = 6;
                    int min = 1;
                    max -= min;
                    password[i + 1] = (int) (Math.random() * ++max) + min;
                }
            } else {
                while (password[i - 1] + password[i] + password[i + 1] != sum) {
                    int maxValue = 6;
                    int minValue = 1;
                    maxValue -= minValue;
                    password[i + 1] = (int) (Math.random() * ++maxValue) + minValue;
                }
            }
        }
        System.out.println(Arrays.toString(password) + " door opened!");
    }
}
