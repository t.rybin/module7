public class ArmstrongNumbers {
    public static void main(String[] args) {
        int k = 500;
        for (int i = 1; i <=k; i++){
            if (isArmstrongNumber(i)){
                System.out.println(i + " is ArmstrongNumber");
            }
        }
    }
    public static boolean isArmstrongNumber(int number){
        int originalNumber = number;
        int digits = (int) Math.log10(number)+1;
        int sum = 0;
        while (number>0){
            int lastDigit = number%10;
            sum += Math.pow(lastDigit, digits);
            number/=10;
        }
        return originalNumber==sum;
    }
}
